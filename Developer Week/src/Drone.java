public class Drone {
    private int posX;
    private int posY;
    private int energy;
    private boolean Cup;

    public Drone(int posX, int posY, int energy) {
        this.posX = posX; // La posizione iniziale del drone verrà impostata dopo l'INIT
        this.posY = posY;
        this.energy = energy;
        this.Cup = false;
    }
    // Getter for posX
    public int getPosX() {
        return posX;
        
    }

    // Setter for posX
    public void setPosX(int posX) {
        this.posX = posX;
    }

    // Getter for posY
    public int getPosY() {
        return posY;
    }

    // Setter for posY
    public void setPosY(int posY) {
        this.posY = posY;
    }
    public int getEnergy() {
        return energy;
    }

    // Setter for posY
    public void setEnergy(int energy) {
        this.energy = energy;
    }

    // Getter for hasCup
    public boolean hasCup() {
        return Cup;
    }

    // Setter for hasCup
    public void setHasCup(boolean hasCup) {
        this.Cup = hasCup;
    }

    public Integer decideDirectionBasedOnLook(int[] neighbors, int currentDir) {
       
        // L'ordine di preferenza per la regola della mano destra: destra, avanti, sinistra, indietro.
        int[] preferenceOrder = {1, 0, 3, 2}; //1= destra ; 0=avanti ; 3=sinistra ;2 =indietro
        int dir = 0;

        //prima loop per trovare la direzione della coppa( SOLO NEI CASA QUANDO la funzione isCupNear rilascia TRUE)
        for (int i : preferenceOrder) {
            dir= (i + currentDir)%4;
            //in questo modo riusciamo a trovare sempre la destra in base alla nostra direzione
            if (neighbors[dir]== 3 ){ // Se la cella nella direzione specificata è vuota(signfica che possiamo andare con il drone) e quindi return di quella direzione.
                return dir;
                //nord è 0 east 2 south 4 ovest 6
            }
        }
        for (int i : preferenceOrder) {
            dir= (i + currentDir)%4;
           //in questo modo riusciamo a trovare sempre la destra in base alla nostra direzione
            if (neighbors[dir]== 0 || neighbors[dir] == 2){ // Se la cella nella direzione specificata è vuota(signfica che possiamo andare con il drone) e quindi return di quella direzione.
                    return dir;
                //nord è 0 east 2 south 4 ovest 6
            }
        }
        return null;

        }

        public  Boolean isCupNear(int[] neighbors) {

            int[] Order = {1, 0, 3, 2}; // controlliamo tutti i vicini
    
            for (int direction : Order) {
                if (neighbors[direction] == 3) { // Se la cella nella direzione indicata è la coppa ritorniamo true
                    return true;
                }
            }
            return false;
        }
        public  void updateDrone(int newposx, int newposy, int newenergy) {
            setPosX(newposx);
            setPosY(newposy);
            setEnergy(newenergy);
    
        }
}
