import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.Random;

public class MazePanel extends JPanel{
    
    private final int BORDER_THICKNESS = 3;
    private int CELL_SIZE = 20;
    private int posx,posy;
    private Labyrinth labyrinth;
    private FormListener formListener;
    Random rnd = new Random();

    public MazePanel(int width,int height) {
        this.labyrinth = new Labyrinth(width,height);
        setPreferredSize(new Dimension(width * 20 + 1 * CELL_SIZE, height * 20 + 2 * CELL_SIZE));
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        //Creation of  the Maze
        if (labyrinth != null) {
            int cellWidth = labyrinth.getWidth();
            int cellHeight = labyrinth.getHeight();

            //...
            for (int i = 0; i < labyrinth.getWidth(); i++) {
                for (int j = 0; j < labyrinth.getHeight(); j++){

                    if (labyrinth.getCell(j, i) == 1) {
                        g.setColor(Color.BLACK);
                        g.fillRect(j * CELL_SIZE, i * CELL_SIZE, CELL_SIZE, CELL_SIZE);
                    } else if (labyrinth.getCell(j, i) == 0) {
                        g.setColor(Color.WHITE);
                        g.fillRect(j * CELL_SIZE, i * CELL_SIZE,CELL_SIZE, CELL_SIZE);
                    } else if (labyrinth.getCell(j, i) == 2) {
                        g.setColor(Color.GREEN);
                        g.fillRect(j * CELL_SIZE, i * CELL_SIZE, CELL_SIZE, CELL_SIZE);
                    } else if (labyrinth.getCell(j, i) == 3) {
                        g.setColor(Color.RED);
                        g.fillRect(j * CELL_SIZE, i * CELL_SIZE, CELL_SIZE, CELL_SIZE);
                    }else if(labyrinth.getCell(j, i) == 4){
                        g.setColor(Color.LIGHT_GRAY);
                        g.fillRect(j * CELL_SIZE, i * CELL_SIZE, CELL_SIZE, CELL_SIZE);
                    
                    }
                    if (j == posx && i == posy) {
                        g.setColor(Color.BLUE);
                        g.fillOval(j * CELL_SIZE, i * CELL_SIZE, CELL_SIZE, CELL_SIZE);
                    }
                }
            }
            repaint();
        }
    }

    public void setMaze(Labyrinth maze){
        this.labyrinth = maze;
    }
    public void setDrone(int x,int y){
        this.posx = x;
        this.posy = y;
    }
    public static void main(String args[]){
        Labyrinth maze;
                        Drone drone;
                        Rest rest = new Rest();
                        String seed = "";
                        String responseINIT = rest.init(seed);
                    if(!responseINIT.contains("seed not found")){
                        System.out.println(responseINIT); //test per vedere la risposta della INIT
                        
                        //separo la risposte della init in molte variabili
        
                        responseINIT = responseINIT.replaceAll("[{}\"]", ""); // Remove curly braces and quotes
                        String[] keyValuePairs = responseINIT.split(","); // Split by comma to get key-value pairs
        
                        Integer Energy = 0;
                        Integer posx = 0;
                        Integer posy = 0;
                        Integer width = 0;
                        Integer height = 0 ;
                        String team = "";
        
                        for (String pair : keyValuePairs) {
                            String[] entry = pair.split(":"); // Split by colon to separate key and value
                            switch (entry[0]) {
                                case "seed":
                                    seed = entry[1];
                                    break;
                                case "Energy":
                                    Energy = Integer.parseInt(entry[1]);
                                    break;
                                case "posx":
                                    posx = Integer.parseInt(entry[1]);
                                    break;
                                case "posy":
                                    posy = Integer.parseInt(entry[1]);
                                    break;
                                case "width":
                                    width = Integer.parseInt(entry[1]);
                                    break;
                                case "height":
                                    height = Integer.parseInt(entry[1]);
                                    break;
                                case "team":
                                    team = entry[1];
                                    break;
                            }
                        }
        
                        System.out.println("Seed: " + seed);
                        System.out.println("Energy: " + Energy);
                        System.out.println("Position X: " + posx);
                        System.out.println("Position Y: " + posy);
                        System.out.println("Width: " + width);
                        System.out.println("Height: " + height);
                        System.out.println("Team: " + team);
        
                        maze = new Labyrinth(width,height);
                        MazePanel panel2 = new MazePanel(width,height);

                        JFrame frame = new JFrame();
                        frame.add(panel2);
                        frame.setSize(panel2.getPreferredSize());
                        frame.setLocationRelativeTo(null);
                        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                        frame.setResizable(false);
                        frame.setVisible(true);



                        drone = new Drone(posx,posy,Energy);
        
                        int currentDir=0;
                        // ci servirà per effettuare correttamente la strategia della mano destra in base all'ultima posizione
                        //risulta uguale a 0 perchè all'inizio
        
                        boolean cupFound = false;
                        //iterazione finchè non si trova la coppa oppure l'energia del drone finisce
                        
                            while (drone.getEnergy() > 0) {
                                String responseLook = rest.look( seed);
                                System.out.println(responseLook);
        
                                String[] parts = responseLook.split("[\\[\\]]")[1].split(",\\s*");
                                int[] neighbors = new int[parts.length];
                                for (int i = 0; i < parts.length; i++) {
                                    neighbors[i] = Integer.parseInt(parts[i]);
                                }
                                panel2.labyrinth.updateLabyrinth(neighbors,panel2.labyrinth, drone);
                                
                                

                                // funzione per decidere la direzione da far intraprendere al drone. Usiamo la strategia della regola della mano destra
                                Integer dir = drone.decideDirectionBasedOnLook(neighbors, currentDir);
                                currentDir= dir;
        
                                String responseMove = rest.move(seed, dir*2);
        
                                
                                responseMove = responseMove.replaceAll("[{}\"]", ""); // Remove curly braces and quotes
                                String[] keyValuePairsMove = responseMove.split(","); // Split by comma to get key-value pairs
                                Integer newposx = null;
                                Integer newposy = null;
                                Integer newenergy = null;
                                for (String pair : keyValuePairsMove) {
                                    String[] entry = pair.split(":"); // Split by colon to separate key and value
                                    switch (entry[0]) {
                                        case "posx":
                                            newposx = Integer.parseInt(entry[1]);
                                            break;
                                        case "posy":
                                            newposy = Integer.parseInt(entry[1]);
                                            break;
                                        case "Energy":
                                            newenergy = Integer.parseInt(entry[1]);
                                            break;
        
                                    }}
                                // Aggiorna il drone in base alla risposta di MOVE
                                drone.updateDrone( newposx, newposy, newenergy);
                                panel2.setDrone(newposx, newposy);
        
                                // Verifica se la Coppa è vicina e prova a raccoglierla
                                if (drone.isCupNear(neighbors)) {
                                    dir = drone.decideDirectionBasedOnLook(neighbors, currentDir);
                                    currentDir= dir;
                                    rest.move(seed, dir*2);
                                    String responseLoad = rest.load(seed);
                                    System.out.println(responseLoad);
                                    System.out.println("Coppa trovata e raccolta con successo!");
                                    cupFound = true;
                                    break; // Esci dal ciclo una volta trovata e raccolta la coppa
                                }
                                
                            }

                        }
                    }
}
