import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;


public class Frame extends JFrame{

    private PanelStart panel1 = new PanelStart();
    private PanelLose panel3 = new PanelLose();
    private PanelEnd panel4 = new PanelEnd();

    public Frame() throws IOException, InterruptedException{

        super("Harry Potter and the Doom Drone"); // Titolo della finestra

        panel1.setFormListener(new FormListener(){
            @Override
            public void formEventListener(FormEvent fe){
                        Labyrinth maze;
                        Drone drone;
                        Rest rest = new Rest();
                        String seed = fe.getSeed();
                        String responseINIT = rest.init(seed);
                    if(!responseINIT.contains("seed not found")){
                        System.out.println(responseINIT); //test per vedere la risposta della INIT
                        
                        //separo la risposte della init in molte variabili
        
                        responseINIT = responseINIT.replaceAll("[{}\"]", ""); // Remove curly braces and quotes
                        String[] keyValuePairs = responseINIT.split(","); // Split by comma to get key-value pairs
        
                        Integer Energy = 0;
                        Integer posx = 0;
                        Integer posy = 0;
                        Integer width = 0;
                        Integer height = 0 ;
                        String team = "";
        
                        for (String pair : keyValuePairs) {
                            String[] entry = pair.split(":"); // Split by colon to separate key and value
                            switch (entry[0]) {
                                case "seed":
                                    seed = entry[1];
                                    break;
                                case "Energy":
                                    Energy = Integer.parseInt(entry[1]);
                                    break;
                                case "posx":
                                    posx = Integer.parseInt(entry[1]);
                                    break;
                                case "posy":
                                    posy = Integer.parseInt(entry[1]);
                                    break;
                                case "width":
                                    width = Integer.parseInt(entry[1]);
                                    break;
                                case "height":
                                    height = Integer.parseInt(entry[1]);
                                    break;
                                case "team":
                                    team = entry[1];
                                    break;
                            }
                        }
        
                        System.out.println("Seed: " + seed);
                        System.out.println("Energy: " + Energy);
                        System.out.println("Position X: " + posx);
                        System.out.println("Position Y: " + posy);
                        System.out.println("Width: " + width);
                        System.out.println("Height: " + height);
                        System.out.println("Team: " + team);
        
                        maze = new Labyrinth(width,height);
                        MazePanel panel2 = new MazePanel(width,height);

                            setVisible(false);
                            remove(panel1);
                            setVisible(true);
                            add(panel2);
                        
                        setSize(new Dimension(width * 20 + 2 * 20, height * 20 + 2 * 20));
                        
                        drone = new Drone(posx,posy,Energy);
        
                        int currentDir=0;
                        // ci servirà per effettuare correttamente la strategia della mano destra in base all'ultima posizione
                        //risulta uguale a 0 perchè all'inizio
        
                        boolean cupFound = false;
                        //iterazione finchè non si trova la coppa oppure l'energia del drone finisce
                        
                            while (drone.getEnergy() > 0) {
                                String responseLook = rest.look( seed);
                                System.out.println(responseLook);
        
                                String[] parts = responseLook.split("[\\[\\]]")[1].split(",\\s*");
                                int[] neighbors = new int[parts.length];
                                for (int i = 0; i < parts.length; i++) {
                                    neighbors[i] = Integer.parseInt(parts[i]);
                                }
                                maze.updateLabyrinth(neighbors,maze, drone);
                                panel2.setMaze(maze);
                                

                                // funzione per decidere la direzione da far intraprendere al drone. Usiamo la strategia della regola della mano destra
                                Integer dir = drone.decideDirectionBasedOnLook(neighbors, currentDir);
                                currentDir= dir;
        
                                String responseMove = rest.move(seed, dir*2);
        
                                
                                responseMove = responseMove.replaceAll("[{}\"]", ""); // Remove curly braces and quotes
                                String[] keyValuePairsMove = responseMove.split(","); // Split by comma to get key-value pairs
                                Integer newposx = null;
                                Integer newposy = null;
                                Integer newenergy = null;
                                for (String pair : keyValuePairsMove) {
                                    String[] entry = pair.split(":"); // Split by colon to separate key and value
                                    switch (entry[0]) {
                                        case "posx":
                                            newposx = Integer.parseInt(entry[1]);
                                            break;
                                        case "posy":
                                            newposy = Integer.parseInt(entry[1]);
                                            break;
                                        case "Energy":
                                            newenergy = Integer.parseInt(entry[1]);
                                            break;
        
                                    }}
                                // Aggiorna il drone in base alla risposta di MOVE
                                drone.updateDrone( newposx, newposy, newenergy);
        
                                // Verifica se la Coppa è vicina e prova a raccoglierla
                                if (drone.isCupNear(neighbors)) {
                                    dir = drone.decideDirectionBasedOnLook(neighbors, currentDir);
                                    currentDir= dir;
                                    rest.move(seed, dir*2);
                                    String responseLoad = rest.load(seed);
                                    System.out.println(responseLoad);
                                    System.out.println("Coppa trovata e raccolta con successo!");
                                    cupFound = true;
                                    break; // Esci dal ciclo una volta trovata e raccolta la coppa
                                }
                                
                            }
                            
                            remove(panel2);
                            setSize(400,400);
                            if(drone.getEnergy() > 0){
                                add(panel4);
                            }else{add(panel3);}
                        
                    }else{panel1.seed.setText("Seed not found");}
                
            }
    });


        panel3.setFormListener(new FormListener(){
            @Override
            public void formEventListener(FormEvent fe){
                
                System.exit(0);
                /*try {
                    restart();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }*/
               
            }
        });
        panel4.setFormListener(new FormListener(){
            @Override
            public void formEventListener(FormEvent fe){

                System.exit(0);
                /*try {
                    restart();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }*/

            }
        });
        
        add(panel1);
        setSize(600,600);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        setVisible(true);



    }
    
}

