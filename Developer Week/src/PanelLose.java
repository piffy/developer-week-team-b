import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public class PanelLose extends JPanel {

    private Button bot;
    private Label battery, moves, seed, message, image;
    private FormListener formListener;


    PanelLose() throws IOException, InterruptedException {

        bot = new Button("Close");
        seed = new Label ("Seed:  ");
        moves = new Label ( "Drone Moves:  ");
        battery = new Label ("Battery Remaining: 0 ");
        message = new Label ("Thank for playing");
        image = new Label(new ImageIcon("Developer Week/losee.jpg"));

        bot.setFont(new Font("Close", Font.PLAIN, 15));

        setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();

        setBackground(new Color(255, 255, 255));

        seed.setFont(seed.getFont().deriveFont(20f));
        moves.setFont(moves.getFont().deriveFont(20f));
        battery.setFont(battery.getFont().deriveFont(20f));
        

        gbc.weightx = 0.01;
        gbc.weighty = 0.01;

        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridwidth = 1;
        gbc.anchor = GridBagConstraints.CENTER;
        add(image,gbc);

        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.gridwidth = 1;
        add(battery,gbc);

        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.gridwidth = 1;
        add(moves,gbc);

        gbc.gridx = 0;
        gbc.gridy = 3;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.gridwidth = 1;
        add(seed,gbc);

        gbc.gridx = 0;
        gbc.gridy = 4;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.gridwidth = 1;
        add(bot,gbc);

        gbc.gridx = 0;
        gbc.gridy = 5;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.gridwidth = 1;
        add(message,gbc);


        bot.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e){
                FormEvent formEvent = new FormEvent(this);
                if(formListener != null){
                    formListener.formEventListener(formEvent);
                }
            }
        });
    }

    public void setFormListener(FormListener formListener){
        this.formListener = formListener;
    }


}
