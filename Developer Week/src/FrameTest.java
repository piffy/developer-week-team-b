import javax.swing.JFrame;
import javax.swing.JPanel;

public class FrameTest extends JFrame{
    MazePanel panel = new MazePanel(31, 31);
        public FrameTest(){
            add(panel);
        setSize(panel.getPreferredSize());
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        setVisible(true);
        }
}
