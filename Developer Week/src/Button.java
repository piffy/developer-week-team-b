import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.awt.event.ActionEvent;

public class Button extends JButton {

    public Button(String text) throws IOException, InterruptedException{

        super(text);

        //Propietà del primo bottone
        Border bord1 = BorderFactory.createLineBorder(Color.BLACK);
        Border bord2 = BorderFactory.createRaisedBevelBorder();
        Border bordF = BorderFactory.createCompoundBorder(bord1,bord2);
        setBorder(bordF);
        setPreferredSize(new Dimension(90,30));
    
    }
}
