import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class Rest {
    public String init(String seed){
        String risposta;
        String json;
        try {
            if(seed.equals(null)){
                json = "{\n\t\"team\":\"Los Muchachos\"\n}";
            }else{json = "{\n\t\"team\":\"Los Muchachos\",\n\t\"seed\":\"" + seed + "\"\n}";}
            
            HttpClient httpClient = HttpClient.newHttpClient();
            HttpRequest httpRequest = HttpRequest.newBuilder()
                    .uri(URI.create("https://dw.gnet.it/init"))
                    .POST(HttpRequest.BodyPublishers.ofString(json))
                    .header("Content-Type", "application/json")
                    .build();

            try {
                HttpResponse<String> response =
                        httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
                response.statusCode();
                risposta = (response.body().toString());

                return risposta;
            } catch (IOException | InterruptedException e) {
                return "Errore";
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    public String look(String seed){
        String risposta;
        try {
            String json = "{\n\t\"team\":\"Los Muchachos\",\n\t\"seed\":\"" + seed + "\"\n}";
            HttpClient httpClient = HttpClient.newHttpClient();
            HttpRequest httpRequest = HttpRequest.newBuilder()
                    .uri(URI.create("https://dw.gnet.it/look"))
                    .POST(HttpRequest.BodyPublishers.ofString(json))
                    .header("Content-Type", "application/json")
                    .build();

            try {
                HttpResponse<String> response =
                        httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
                response.statusCode();
                risposta = (response.body().toString());
                return risposta;

            } catch (IOException | InterruptedException e) {
                return "Errore";
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    public String move(String seed,int direzione){
        String risposta;
        try {
            String json = "{\n\t\"team\":\"Los Muchachos\",\n\t\"seed\":\"" + seed + "\",\n\t\"move\":\"" + direzione +"\"\n}";
            HttpClient httpClient = HttpClient.newHttpClient();
            HttpRequest httpRequest = HttpRequest.newBuilder()
                    .uri(URI.create("https://dw.gnet.it/move"))
                    .POST(HttpRequest.BodyPublishers.ofString(json))
                    .header("Content-Type", "application/json")
                    .build();

            try {
                HttpResponse<String> response =
                httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
                response.statusCode();
                risposta = (response.body().toString());
                return risposta;

            } catch (IOException | InterruptedException e) {
                return "Errore";
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    public String load(String seed){
        String risposta;
        try {
            String json = "{\n\t\"team\":\"Los Muchachos\",\n\t\"seed\":\"" + seed + "\"\n}";
            HttpClient httpClient = HttpClient.newHttpClient();
            HttpRequest httpRequest = HttpRequest.newBuilder()
                    .uri(URI.create("https://dw.gnet.it/load"))
                    .POST(HttpRequest.BodyPublishers.ofString(json))
                    .header("Content-Type", "application/json")
                    .build();

            try {
                HttpResponse<String> response =
                        httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
                response.statusCode();
                risposta = (response.body().toString());
                return risposta;

            } catch (IOException | InterruptedException e) {
                return "Errore";
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    public String unload(String seed){
        String risposta;
        try {
            String json = "{\n\t\"team\":\"Los Muchachos\",\n\t\"seed\":\"" + seed + "\"\n}";
            HttpClient httpClient = HttpClient.newHttpClient();
            HttpRequest httpRequest = HttpRequest.newBuilder()
                    .uri(URI.create("https://dw.gnet.it/unload"))
                    .POST(HttpRequest.BodyPublishers.ofString(json))
                    .header("Content-Type", "application/json")
                    .build();

            try {
                HttpResponse<String> response =
                        httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
                response.statusCode();
                risposta = (response.body().toString());
                return risposta;

            } catch (IOException | InterruptedException e) {
                return "Errore";
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}


