import java.util.EventObject;

public class FormEvent extends EventObject{
String seed = "";
    public FormEvent(Object source) {
        super(source);
    }
    public FormEvent(Object source,String seed) {
        super(source);
        this.seed = seed;
    }
    public String getSeed (){
        return seed;
    }
    
}