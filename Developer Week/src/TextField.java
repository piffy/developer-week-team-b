import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class TextField extends JTextField{

    public TextField(){

        //proprietà del textfield
        setCaretColor(Color.BLACK);
        setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
        setPreferredSize(new Dimension(250,25));
    }
}

