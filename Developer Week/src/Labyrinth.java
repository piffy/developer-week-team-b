public class Labyrinth {
    private int width;
    private int height;
    public int[][] cells;

    public Labyrinth(int width, int height) {
        this.width = width;
        this.height = height;
        this.cells = new int[width][height];
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                cells[i][j] = 4;
            }
        }
    }
    public Labyrinth(){

    }

    public void setCellType(int neighbor, int posx, int posy){
        int cell = getCell(posx, posy);
        if (cell != 4) {}
            else if (neighbor == 0) cells[posx][posy] = 0;
            else if (neighbor == 1) cells[posx][posy] = 1;
            else if (neighbor == 2) cells[posx][posy] = 2;
            else if (neighbor == 3) cells[posx][posy] = 3;
        
        

    }
    public int getCell(int x, int y) {
        if (x >= 0 && x < width && y >= 0 && y < height) {
            return cells[x][y];
        } else {
            return -1;
        }
    }

    //Questo metodo modifica la direzione in modo che il drone preferisca visitare celle non ancora visitate, evitando di entrare in loop
    
    public void updateLabyrinth(int[] neighbors, Labyrinth maze, Drone drone) {

        int dronex=drone.getPosX();
        int droney=drone.getPosY();
        for(int i=0;i<4;i++){
            if(i==0)//significa che la nuova cella da settare nel labirinto è quella posizionata in alto rispetto alla posizione del drone
                maze.setCellType(neighbors[i], dronex, droney++);
            if(i==1)//significa che la nuova cella da settare nel labirinto è quella posizionata a destra rispetto alla posizione del drone
                maze.setCellType(neighbors[i], dronex++, droney);
            if(i==2) //significa che la nuova cella da settare nel labirinto è quella posizionata in basso rispetto alla posizione del drone
                maze.setCellType(neighbors[i], dronex, droney--);
            if(i==3) //significa che la nuova cella da settare nel labirinto è quella posizionata a sinistrarispetto alla posizione del drone
                maze.setCellType(neighbors[i], dronex--, droney);

        }
    }
    public int getWidth(){
        return width;
    }
    public int getHeight(){
        return height;
    }
    public void setHeight(int height){
        this.height = height;
    }
    public void setWidth(int width){
        this.width = width;
    }

}