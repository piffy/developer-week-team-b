import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public class PanelStart extends JPanel {

    private Label text, title;
    public TextField seed;
    private Button bot;
    private FormListener formListener;

    PanelStart() throws IOException, InterruptedException {

        setLayout(new GridBagLayout());

        bot = new Button("Start");
        seed = new TextField();
        text = new Label ("Seed:  ");
        title = new Label ("Harry Potter and the Doom Drone");
        bot.setFont(new Font("Start", Font.PLAIN, 15));

        title.setFont(title.getFont().deriveFont(27f));
        seed.setFont(seed.getFont().deriveFont(17f));
        
        bot.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e){
                String seed1 = seed.getText();
                FormEvent formEvent = new FormEvent(this,seed1);
                if(formListener != null){
                    formListener.formEventListener(formEvent);
                }
            }
        });


        GridBagConstraints gbc = new GridBagConstraints();
        setBackground(new Color(255, 255, 255));

        gbc.weightx = 0.01;
        gbc.weighty = 0.01;

        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridwidth = 2;
        gbc.anchor = GridBagConstraints.CENTER;
        add(title,gbc);

        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.anchor = GridBagConstraints.LINE_END;
        gbc.gridwidth = 1;
        add(text,gbc);

        gbc.gridx = 1;
        gbc.gridy = 1;
        gbc.anchor = GridBagConstraints.LINE_START;
        gbc.gridwidth = 1;
        add(seed,gbc);

        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.gridwidth = 2;
        add(bot,gbc);


    }
    public void setFormListener(FormListener formListener){
        this.formListener = formListener;
    }
}
