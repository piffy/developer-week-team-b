import javax.swing.*;
import java.awt.*;

public class Label extends JLabel {

    public Label(String text){

        super(text);

        //Proprietà del label
        setAlignmentX(Component.RIGHT_ALIGNMENT);
        setBorder(BorderFactory.createEmptyBorder(0,0,0,0));
    }

    public Label(Icon image){

        super(image);

    }

}
